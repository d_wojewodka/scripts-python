import os
import glob
import binascii

prb = "gr2 >> object"
prd = "gr2 >> dungeon block"
prt = "spt >> tree"
pre = "mse >> effect"
bshadow = ''
filetype = [prb, prd, prt, pre]
filename = []
filepath = []
crc32 = []
filecount = 0
currentfile = 0
count = 0

def clear():
    os.system('cls')


def getcrc32(f):
    fo = open(f, 'rb').read()
    return binascii.crc32(fo) % (1 << 32)


def properties(fileext, propertyext, propetytype):
    global currentfile, filecount, count
   
    for srcfn in glob.glob('*%s' % fileext):
        if '_lod_0' in srcfn:  # skip LOD files
            pass
        else:
            filename.append(srcfn)
            filepath.append((os.getcwd() + '\%s' % srcfn).replace('\\', '/').lower())
            filecount += 1

    for i in range(0, len(filename)):
        prop = open(filename[i][:-4] + propertyext, 'w')
        prop.write('YPRT\n')
        crc32.append(getcrc32(filename[i]))
        prop.write(str(crc32[i]) + '\n')
        if propertyext == '.prb':
            prop.write('buildingfile\t\t"%s"' % filepath[i].lower())
            if os.path.exists(filename[i][:-4] + '.mdatr'):
                prop.write('\nisattributedata\t\t"0"')  # include .mdatr
        if propertyext == '.prd':
            prop.write('dungeonblockfile\t\t"%s"' % filepath[i].lower())
        if propertyext == '.prt':
            prop.write('treefile\t\t"%s"' % filepath[i].lower())
        if propertyext == '.pre':
            prop.write('effectfile\t\t"%s"' % filepath[i].lower())
        prop.write('\npropertyname\t\t"%s"\n' % filename[i][:-4])
        prop.write('propertytype\t\t"%s"\n' % propetytype)
        if propertyext == '.prb' and bshadow == 'T':
            prop.write('shadowflag\t\t"1"\n')  # include shadow
        if propertyext == '.prt':
            prop.write('treesize\t\t"1000.000000"\n')
            prop.write('treevariance\t\t"0.000000"')

        print "(%d/%d) %s | crc32: %d" % (currentfile + 1, filecount, filename[i], crc32[i])
        currentfile += 1
        count += 1

      
def option(ext):
    global bshadow

    if ext == prb:
        outputshadow = raw_input("Include shadows? (T/N): ")
        if outputshadow == 't' or outputshadow == 'T':
            bshadow += 'T'
        properties('.gr2', '.prb', 'Building')
    if ext == prd:
        properties('.gr2', '.prd', 'DungeonBlock')
    if ext == prt:
        properties('.spt', '.prt', 'Tree')
    if ext == pre:
        properties('.mse', '.pre', 'Effect')


def menu():
    print "1) ", prb
    print "2) ", prd
    print "3) ", prt
    print "4) ", pre
    select = raw_input("Choose option: ")
    if 5 > int(select) > 0:
        clear()
        print "You have chosen: ", filetype[int(select) - 1]
        option(filetype[int(select) - 1])
    else:
        clear()
        menu()


menu()
print "Generated", count
raw_input("Press any key to exit")
