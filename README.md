## batchPropertyGenerator -> script for generating property files for game Metin2 to speed up the process (normally you have to do it one by one).
Features: (requires Python2.7)
- crc32 as ObjectID (same as in WE)
- skip LOD file (.gr2)
- adjustable shadow output (Buildings)
- auto .mdatr attachment (Collisions for *.gr2)
